module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production' ? '/breakout-question/' : '/',
  outputDir: 'dist',
  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
      },
    },
  },
};
